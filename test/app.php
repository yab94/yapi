<?php

include 'vendor/autoload.php';

use Yapi\Conf;
use Yapi\App;
use Yapi\Req;
use Yapi\Db;

Conf::set('authorized.tokenName', 'X-token');
Conf::set('authorized.tokenValue', 'azerty');
Conf::set('database.dsn', 'mysql:host=localhost;dbname=accounting');
Conf::set('database.username', 'accounting');
Conf::set('database.password', 'accounting');
Conf::set('database.driver_options', [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8';"]);

$req = Req::global();
$app = new App();

$app->before(function($app, $req) {

    if($req->getHeader(Conf::get('authorized.tokenName')) != Conf::get('authorized.tokenValue'))
        return Res::unauthorized();

    $app->accountId = 41;

});

$app->get('/api/v1/customers', fn() => $app->json(Db::select('customer', '*')));
$app->post('/api/v1/customers', fn() => $app->insert('customer', [['name', $req->getBodyParam('name'), fn($name) => $name]]));
$app->put('/api/v1/customers/:id', fn($id) => $app->update('customer', [['name', $req->getBodyParam('name'), fn($name) => $name]], ['id' => $id]));
$app->delete('/api/v1/customers/:id', fn($id) => $app->remove('customer', ['id' => $id]));
$app->get('/api/v1/customers/:id', fn($id) => $app->find('customer', ['id' => $id]));

$app->route($req)->flush();