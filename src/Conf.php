<?php

namespace Yapi;

class Conf {

    static protected $params = [];

    static public function get(string $param, $default = null) {

        if(!isset(self::$params[$param]) && $default === null)
            throw new \Exception('missing config param "'.$param.'"');

        return self::$params[$param] ?? $default;

    }

    static public function set(string $param, $value): void {

        self::$params[$param] = $value;

    }

}