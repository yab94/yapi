<?php

namespace Yapi;

class Db extends \PDO{

    static private $instance = null;

    static public function instance(): Db {

        if(self::$instance === null) {

            self::$instance = new self(
                Conf::get('database.dsn'),
                Conf::get('database.username'),
                Conf::get('database.password'),
                Conf::get('database.driver_options')
            );

        }

        return self::$instance;

    }

    static public function andWhereEq(array $filters): string {

        if(!count($filters))
            return '';

        foreach($filters as $column => $value)
            $filters[$column] = self::identifier($column).' = '.self::escape($value);

        return ' WHERE '.implode(' AND ', $filters);

    }

    static public function identifier(string $expression): string {

        return '`'.$expression.'`';

    }

    static public function escape(string $expression): string {

        return self::instance()->quote($expression);

    }

    static public function error(): string {

        return implode(' ', self::instance()->errorInfo());
        
    }

    static public function insert(string $table, array $datas): int {

        foreach($datas as $field => $value) 
            $datas[$field] = self::identifier($field).' = '.self::escape($value);
        
        $sql = 'INSERT INTO '.self::identifier($table).' ('.implode(', ', array_map([Db::class, 'identifier'], array_keys($datas))).') VALUES ('.implode(', ', array_map([Db::class, 'escape'], $datas)).')';
        
        Log::debug('Db::insert '.$sql);

        $result = self::instance()->exec($sql);
        
        if($result === false)
            throw new \PDOException(self::error());

        return $result;

    }

    static public function update(string $table, array $datas, array $filters): int {

        foreach($datas as $field => $value) 
            $datas[$field] = self::identifier($field).' = '.self::escape($value);

        $sql = 'UPDATE '.self::identifier($table).' SET '.implode(', ', $datas).self::andWhereEq($filters);

        Log::debug('Db::update '.$sql);

        $result = self::instance()->exec($sql);
        
        if($result === false)
            throw new \PDOException(self::error());

        return $result;

    }

    static public function delete(string $table, array $filters): int {

        $sql = 'DELETE FROM '.self::identifier($table).self::andWhereEq($filters);

        Log::debug('Db::delete '.$sql);

        $result = self::instance()->exec($sql);
        
        if($result === false)
            throw new \PDOException(self::error());

        return $result;

    }

    static public function select(string $table, string $fields = '*', array $filters = [], array $orders = [], int $limit = 0, int $offset = null): \PDOStatement {
        
        $select = 'SELECT '.$fields.' FROM '.self::identifier($table).self::andWhereEq($filters);

        if(count($orders)) {

            $where = [];

            foreach($orders as $column => $direction)
                $orders[$column] = self::identifier($$column).' '.$direction;

            $select .= ' ORDER BY '.implode(', ', $orders);

        }

        if($offset !== null) 
            $select .= ' LIMIT '.$limit.', '.$offset;

        $stmt = self::instance()->prepare($select);
        
        if($stmt === false)
            throw new \PDOException(self::error());

        $stmt->setFetchMode(\PDO::FETCH_ASSOC);

        Log::debug('Db::select '.$stmt->queryString);

        $stmt->execute();

        return $stmt;

    }

    static public function find(string $table, $fields = '*', array $filters): array {

        $select = 'SELECT '.$fields.' FROM '.self::identifier($table).self::andWhereEq($filters);

        $stmt = self::instance()->prepare($select);
        
        if($stmt === false)
            throw new \PDOException(self::error());

        $stmt->setFetchMode(\PDO::FETCH_ASSOC);

        Log::debug('Db::find '.$stmt->queryString);

        $stmt->execute();

        $found = null;

        foreach($stmt as $datas) {

            if($found === null) {

                $found = $datas;

            } else {

                throw new \Exception('can not find because filters match more than one row');

            }

        }

        return $found;

    }
    
}