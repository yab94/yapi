<?php

namespace Yapi;

if(!defined('LOG_LOCAL0'))
    define('LOG_LOCAL0', 0);

class Log {

    const LOG_EMERG = 0;
    const LOG_ALERT = 1;
    const LOG_CRIT = 2;
    const LOG_ERR = 3;
    const LOG_WARNING = 4;
    const LOG_NOTICE = 5;
    const LOG_INFO = 6;
    const LOG_DEBUG = 7;

    static protected $priorities = [
        self::LOG_EMERG => 'LOG_EMERG', // 	système inutilisable
        self::LOG_ALERT => 'LOG_ALERT', // 	une décision doit être prise immédiatement
        self::LOG_CRIT => 'LOG_CRIT', // 	condition critique
        self::LOG_ERR => 'LOG_ERR', // 	condition d'erreur
        self::LOG_WARNING => 'LOG_WARNING', // 	condition d'alerte
        self::LOG_NOTICE => 'LOG_NOTICE', // 	condition normale, mais significative
        self::LOG_INFO => 'LOG_INFO', // 	message d'information
        self::LOG_DEBUG => 'LOG_DEBUG', // 	message de déboguage
    ];

    static protected int $priority = self::LOG_ERR;
    static protected string $ident = '';
    static protected string $prefix = '';
    static protected $options = LOG_CONS | LOG_ODELAY | LOG_PID;
    static protected int $facility = LOG_LOCAL0;

    static protected function write(int $priority, string $message): bool {

        if ($priority < self::$priority) 
            return false;

        openlog(self::$ident . ' ' . self::$prefix, self::$options, self::$facility);

        syslog($priority, self::$priorities[$priority] . ' ' . $message);

        closelog();

        return true;
    }

    static public function setIdent(string $ident): void {

        self::$ident = (string) $ident;
    }

    static public function setPrefix(string $prefix): void {

        self::$prefix = (string) $prefix;
    }

    static public function setOptions($options): void {

        self::$options = $options;
    }

    static public function setFacility(int $facility): void {

        self::$facility = $facility;

    }

    static public function setPriority(int $priority): void {

        self::$priority = (int) $priority;

    }

    static public function debug(string $message): bool {

        return self::write(self::LOG_DEBUG, $message);
    }

    static public function info(string $message): bool {

        return self::write(self::LOG_INFO, $message);
    }

    static public function notice(string $message): bool {

        return self::write(self::LOG_NOTICE, $message);
    }

    static public function warning(string $message): bool {

        return self::write(self::LOG_WARNING, $message);
    }

    static public function error(string $message): bool {

        return self::write(self::LOG_ERR, $message);
    }

    static public function critical(string $message): bool {

        return self::write(self::LOG_CRIT, $message);
    }

    static public function alert(string $message): bool {

        return self::write(self::LOG_ALERT, $message);
    }

    static public function emerg(string $message): bool {

        return self::write(self::LOG_EMERG, $message);
    }

}
    
    