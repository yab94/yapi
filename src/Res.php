<?php

namespace Yapi;

class Res {

    use Http, Tool;

    protected $code = null;

    public function __construct(int $code, string $body = '') {

        $this->setCode($code);

        if($body)
            $this->setBody($body);

    }

    static public function fromString(string $string, $jumpContinue = true): Res {

        $parts = explode("\r\n\r\n", $string);

        $headers = array_shift($parts);
        $body = implode("\r\n\r\n", $parts);

        $headers = explode("\r\n", $headers);

        $firstHeader = array_shift($headers);

		$res = self::fromHttpHeader($firstHeader);

        if($jumpContinue && $res->getCode() == 100)
            return self::fromString($body);

        foreach($headers as $header) {

            $header = explode(':', $header);

            $headerName = array_shift($header);
            $headerValue = implode(':', $header);

            $res->setHeader($headerName, $headerValue);

        }

		$res->body = (string) $body;
		
		return $res;
		
    }

    static public function fromHttpHeader(string $header): Res {

        if(!preg_match('#(http/[0-9\.]+)\s+([0-9]+)\s+.+$#i', $header, $match))
            throw new \Exception('unable to generate response from header "'.$header.'"');

        $res = new self($match[2]);

        $res->setProtocol($match[1]);

        return $res;

    }

    static public function __callStatic($method, array $args): Res {

        return new self(self::codeByMessage($method), ...$args);

    }

    public function setCode(int $code): Res {

        if(!isset(self::$codes[$code]))
            throw new \Exception('invalid HTTP code "'.$code.'"');

        $this->code = (int) $code;

        return $this;
    }

    public function getCode(): int {

        return $this->code;
        
    }

    public function flush() {

        if(!headers_sent()) {

            header($this->protocol." ".$this->code." ".self::$codes[$this->code]);

            foreach($this->headers as $headerName => $headerValues) {

                foreach($headerValues as $headerValue) {
                    
                    header(ucwords($headerName, '-').': '.$headerValue);

                }

            }

        }
      
        echo $this->body;

        $this->body = '';

    }

    public function send() {

        $this->flush();

        exit();

    }

}