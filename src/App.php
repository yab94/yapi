<?php

namespace Yapi;

class App {

    protected $routes = [];
    protected $params = [];
    protected $before = [];
    protected $after = [];

    public function before(\Closure ...$closures): App {
        
        foreach($closures as $closure)
            $this->before[] = $closure;

        return $this;

    }

    public function after(\Closure ...$closures): App {
        
        foreach($closures as $closure)
            $this->after[] = $closure;

        return $this;

    }

    public function __set(string $param, $value) {
        
        $this->params[$param] = $value;

    }

    public function __get(string $param) {
        
        if(!isset($this->params[$param]))
            throw new \Exception('missing app param "'.$param.'"');

        return $this->params[$param];

    }

    public function route(Req $req = null, Res $res = null): Res {

        if($req === null)
            $req = Req::fromGlobals();

        if($res === null)
            $res = Res::notFound();

        try {

            foreach($this->before as $before) 
                $before($req, $res);
    
            if(!isset($this->routes[$req->getVerb()]))
                throw new \Exception('no '.$req->getVerb().' route', 405);

            foreach($this->routes[$req->getVerb()] as $route => $middlewares) {
    
                $route = '#^'.preg_replace('#:[^/]+#', '([a-zA-Z0-9\-_]+)', $route).'$#';

                if(!preg_match($route, $req->getPath(), $params))
                    continue;
                
                $res->setCode(200);

                array_shift($params);

                $closure = array_pop($middlewares);

                foreach($middlewares as $middleware) 
                    $middleware($req, $res);

                $closure($req, $res, ...$params);

                break;
    
            }

            foreach($this->after as $after)  
                $after($req, $res);
                
            Log::info($req->getVerb().' '.$req->getUri().' '.$res->getProtocol().' '.$res->getCode());

        } catch(\Exception $e) {

            $res->setCode($e->getCode() ? min(max(399, $e->getCode()), 599) : 500);
            
            Log::error($req->getVerb().' '.$req->getUri().' '.$req->getBody().' '.$res->getProtocol().' '.$res->getCode().' '.$res->getBody());

        }
            
        return $res;

    }

    public function get(string $route, \Closure ...$closures): App {

        $this->routes['GET'][$route] = $closures;

        return $this;

    }

    public function post(string $route, \Closure ...$closures): App {

        $this->routes['POST'][$route] = $closures;

        return $this;

    }

    public function put(string $route, \Closure ...$closures): App {

        $this->routes['PUT'][$route] = $closures;

        return $this;

    }

    public function delete(string $route, \Closure ...$closures): App {

        $this->routes['DELETE'][$route] = $closures;

        return $this;

    }

    protected function apply(array $rules): array {

        $datas = [];
        $errors = [];

        foreach($rules as $index => $rule) {

            if(!is_array($rule)) {

                $datas[$index] = $rule;
                
                continue;

            }

            $field = array_shift($rule);
            $value = array_shift($rule);
            $closure = array_shift($rule);

            if(($closure instanceof \Closure) && !$closure($value))
                $errors[] = $field;

            $datas[$field] = $value;

        }

        if(count($errors))
            throw new \OutOfBoundsException('bad value for field: '.implode(', ', $errors));

        return $datas;

    }

    public function insert(Req $req, Res $res, $table, array $rules): Res {

        return $res->setCode(Db::insert($table, $this->apply($rules)) ? 201 : 304);

    }

    public function update(Req $req, Res $res, $table, array $rules, array $filters): Res {

        return $res->setCode(Db::update($table, $this->apply($rules), $filters) ? 204 : 304);

    }

    public function remove(Req $req, Res $res, $table, array $filters): Res {

        return $res->setCode(Db::delete($table, $filters) ? 204 : 304);

    }

    public function find(Req $req, Res $res, $table, array $filters): Res {

        $select = Db::select($table, '*', $filters);

        $data = $select->fetch();

        if(!$data)
            throw new \Exception('can not find item "'.$table.'"', 404);

        return $res->setBodyParams($data);

    }

    public function swagger(Req $req, Res $res): Res {

        $swagger = [];

        foreach($this->routes as $verb => $routes) {

            foreach($routes as $route => $middlewares) {

                if(preg_match('#^'.preg_replace('#:[^/]+#', '([a-zA-Z0-9\-_]+)', $route).'$#', $req->getUri(), $params))
                    continue;
                    
                preg_match_all('#(:[^/]+)#', $route, $matches);

                $swagger[] = [
                    'verb' => $verb,
                    'route' => $route,
                    'params' => $matches[1],
                ];

            }

        }

        return $res->setBodyParams($swagger);

    }
    
}