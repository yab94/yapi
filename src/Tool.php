<?php

namespace Yapi;

trait Tool {

    static public function humanReadableSize(float $value): string {

        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

        for ($i = 0; $value > 1024; $i++)
            $value /= 1024;

        return round($value, 2) . ' ' . $units[$i];
    }

    static public function snakeCase(string $text): string {

        preg_match_all('#[^a-z0-9]+#', $text, $match);

        foreach ($match[0] as $value)
            $text = str_replace($match[0], '_' . strtolower($match[0]), $text);

        return $text;
    }

    static public function pascalCase(string $text): string {

        preg_match_all('#[^a-zA-Z0-9]+([a-zA-Z0-9]?)#', $text, $match);

        foreach ($match[0] as $key => $value)
            $text = str_replace($value, ucfirst($match[1][$key]), $text);

        return ucfirst($text);
    }

    static public function camelCase(string $text): string {

        preg_match_all('#[^a-zA-Z0-9]+([a-zA-Z0-9]?)#', $text, $match);

        foreach ($match[0] as $key => $value)
            $text = str_replace($value, ucfirst($match[1][$key]), $text);

        return lcfirst($text);
    }

    static public function singularize(string $text): string {

        $singulars = [
            '/(quiz)zes$/i' => '$1',
            '/(matr)ices$/i' => '$1ix',
            '/(vert|ind)ices$/i' => '$1ex',
            '/^(ox)en$/i' => '$1',
            '/(alias)es$/i' => '$1',
            '/(octop|vir)i$/i' => '$1us',
            '/(cris|ax|test)es$/i' => '$1is',
            '/(shoe)s$/i' => '$1',
            '/(o)es$/i' => '$1',
            '/(bus)es$/i' => '$1',
            '/([m|l])ice$/i' => '$1ouse',
            '/(x|ch|ss|sh)es$/i' => '$1',
            '/(m)ovies$/i' => '$1ovie',
            '/(s)eries$/i' => '$1eries',
            '/([^aeiouy]|qu)ies$/i' => '$1y',
            '/([lr])ves$/i' => '$1f',
            '/(tive)s$/i' => '$1',
            '/(hive)s$/i' => '$1',
            '/(li|wi|kni)ves$/i' => '$1fe',
            '/(shea|loa|lea|thie)ves$/i' => '$1f',
            '/(^analy)ses$/i' => '$1sis',
            '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '$1$2sis',
            '/([ti])a$/i' => '$1um',
            '/(n)ews$/i' => '$1ews',
            '/(h|bl)ouses$/i' => '$1ouse',
            '/(corpse)s$/i' => '$1',
            '/(us)es$/i' => '$1',
            '/s$/i' => '',
        ];

        $irregulars = [
            'move' => 'moves',
            'foot' => 'feet',
            'goose' => 'geese',
            'sex' => 'sexes',
            'child' => 'children',
            'man' => 'men',
            'tooth' => 'teeth',
            'person' => 'people',
        ];

        $uncountables = [
            'sheep',
            'fish',
            'deer',
            'series',
            'species',
            'money',
            'rice',
            'information',
            'equipment',
        ];

        if (in_array(strtolower($text), $uncountables))
            return $text;

        foreach ($irregulars as $pattern => $result) {

            $pattern = '/' . $pattern . '$/i';

            if (preg_match($pattern, $text))
                return preg_replace($pattern, $result, $text);
        }

        foreach ($singulars as $pattern => $result) {

            if (preg_match($pattern, $text))
                return preg_replace($pattern, $result, $text);
        }

        return $text;
    }

    static public function pluralize(string $text, int $nb = null): string {

        if($nb === 0 || $nb === 1)
            return $text;

        $plurals = [
            '/(quiz)$/i' => '$1zes',
            '/^(ox)$/i' => '$1en',
            '/([m|l])ouse$/i' => '$1ice',
            '/(matr|vert|ind)ix|ex$/i' => '$1ices',
            '/(x|ch|ss|sh)$/i' => '$1es',
            '/([^aeiouy]|qu)y$/i' => '$1ies',
            '/(hive)$/i' => '$1s',
            '/(?:([^f])fe|([lr])f)$/i' => '$1$2ves',
            '/(shea|lea|loa|thie)f$/i' => '$1ves',
            '/sis$/i' => 'ses',
            '/([ti])um$/i' => '$1a',
            '/(tomat|potat|ech|her|vet)o$/i' => '$1oes',
            '/(bu)s$/i' => '$1ses',
            '/(alias)$/i' => '$1es',
            '/(octop)us$/i' => '$1i',
            '/(ax|test)is$/i' => '$1es',
            '/(us)$/i' => '$1es',
            '/s$/i' => 's',
            '/$/' => 's',
        ];

        $irregulars = [
            'move' => 'moves',
            'foot' => 'feet',
            'goose' => 'geese',
            'sex' => 'sexes',
            'child' => 'children',
            'man' => 'men',
            'tooth' => 'teeth',
            'person' => 'people',
        ];

        $uncountables = [
            'sheep',
            'fish',
            'deer',
            'series',
            'species',
            'money',
            'rice',
            'information',
            'equipment',
        ];

        if (in_array(strtolower($text), $uncountables))
            return $text;

        foreach ($irregulars as $pattern => $result) {

            $pattern = '/' . $pattern . '$/i';

            if (preg_match($pattern, $text))
                return preg_replace($pattern, $result, $text);
        }

        foreach ($plurals as $pattern => $result) {

            if (preg_match($pattern, $text))
                return preg_replace($pattern, $result, $text);
        }

        return $text;
    }

    static public function dateFormat(string $expression = '', string $format = "%A %d %B %Y - %Hh%M"): string {
      
        setlocale(LC_TIME, 'fr_FR.utf8', 'fra'); 
        
        if(preg_match('/^([0-9]{4})-([0-9]{2})-([0-9]{2})\s+([0-9]{2}):([0-9]{2}):([0-9]{2})$/is', $expression, $date)) {
    
          $timestamp = mktime($date[4], $date[5], $date[6], $date[2], $date[3], $date[1]);
    
        } elseif(!$expression) {
        
          $timestamp = time();
        
        } else {
        
          $timestamp = intval($expression);
            
        }
    
        return strftime($format, $timestamp); 

    }

    static public function unparseUrl(array $parsedUrl): string {

        $scheme   = isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . '://' : '';
        $host     = isset($parsedUrl['host']) ? $parsedUrl['host'] : '';
        $port     = isset($parsedUrl['port']) ? ':' . $parsedUrl['port'] : '';
        $user     = isset($parsedUrl['user']) ? $parsedUrl['user'] : '';
        $pass     = isset($parsedUrl['pass']) ? ':' . $parsedUrl['pass']  : '';

        $pass     = ($user || $pass) ? "$pass@" : '';

        $path     = isset($parsedUrl['path']) ? $parsedUrl['path'] : '';
        $query    = isset($parsedUrl['query']) ? '?' . $parsedUrl['query'] : '';
        $fragment = isset($parsedUrl['fragment']) ? '#' . $parsedUrl['fragment'] : '';

        return "$scheme$user$pass$host$port$path$query$fragment";

    }

    static public function getChmod(string $path): string {

        if(!is_file($path) && !is_dir($path))
            throw new \Exception('can not detect chmod if path dont exists');

        return substr(sprintf('%o', fileperms($path)), -4);

    }
  
	static public function getLineEnding($file): string {

        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

        if(!is_file($file))
            throw new \Exception('can not detect line ending if file dont exists');

		$part = '';
	
		$fh = fopen($file, 'r');

		if($fh) {

			$part = fread($fh, 8192);

			fclose($fh);
			
        }

        $lineEndings = self::getLineEndings();
        
        foreach($lineEndings as $lineEnding) {

            if(strpos($part,  $lineEnding['char']))
                return $lineEnding;

        }

        return $lineEndings[3];

	}
  
	static public function getLineEndings(): array {

        return [
            1 => ['id' => 1, 'label' => 'Win', 'char' => '\r\n', 'qchar' => "\r\n"],
            2 => ['id' => 2, 'label' => 'Mac', 'char' => '\r', 'qchar' => "\r"],
            3 => ['id' => 3, 'label' => 'Unix', 'char' => '\n', 'qchar' => "\n"],
        ];

	}

}

// Do not clause PHP tags unless it is really necessary
