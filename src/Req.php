<?php

namespace Yapi;

class Req {

    use Http, Tool;

    const DEFAULT_PROTOCOL = "HTTP/1.0";

    protected $verb = '';
    protected $uri = '';
    protected $files = [];

    static public function __callStatic($verb, $arguments): Req {

        return new self(strtoupper($verb), ...$arguments);

    }

    static public function fromGlobals(): Req {

        if(PHP_SAPI == 'cli')
            throw new \Exception('can not use global request in cli-mode', 500);

        $req = new self($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);

        $req->setHeaders(apache_request_headers());

        $req->body = file_get_contents('php://input');

        $req->files = $_FILES;
 
        return $req;

    }

    public function __construct($verb, $uri, array $headers = [], $body = '', $protocol = self::DEFAULT_PROTOCOL) {

        $this->setProtocol($protocol)
            ->setVerb($verb)
            ->setUri($uri)
            ->setHeaders($headers)
            ->setBody($body);

    }

    public function getVerb(): string {

        return $this->verb;

    }

    public function getUri(): string {

        return $this->uri;

    }

    public function getPath(): string {

        $parsedUrl = parse_url($this->uri);

        return $parsedUrl['path'];

    }

    public function setVerb($verb): Req {

        $verb = strtoupper((string) $verb);

        if(!in_array($verb, self::$verbs))
            throw new \Exception('invalid HTTP verb "'.$verb.'"', 500);

        $this->verb = $verb;

        return $this;

    }

    public function getFileParams(): array {
        
        return $this->files;

    }

    public function getFileParam($name, $defaultValue = null): array {

        $files = $this->getFileParams();

        if(!isset($files[$name]) && $defaultValue === null)
            throw new \Exception('invalid file param "'.$name.'"', 400);
        
        return $files[$name] ?? $defaultValue;

    }

    public function setUri($uri): Req {

        $this->uri = (string) $uri;

        return $this;

    }

    public function removeQueryParam($param): Req {

        $params = $this->getQueryParams();

        if(!isset($params[$param]))
            throw new \Exception('can not remove query param "'.$param.'"', 500);

        unset($params[$param]);

        return $this->setQueryParams($params);

    }

    public function removeQueryParams(): Req {

        return $this->setQueryParams([]);

    }

    public function addQueryParams(array $queryParams): Req {

        $params = $this->getQueryParams();

        foreach($queryParams as $param => $value)
            $params[$param] = $value;

        return $this->setQueryParams($params);

    }

    public function addQueryParam($param, $value): Req {

        $params = $this->getQueryParams();

        $params[$param] = $value;

        return $this->setQueryParams($params);

    }

    public function setQueryString($queryString): Req {

        $parsedUrl = parse_url($this->uri);

        $parsedUrl['query'] = $queryString;

        $this->uri = self::unparseUrl($parsedUrl);

        return $this;

    }

    public function setQueryParams(array $params): Req {

        return $this->setQueryString(http_build_query($params));

    }

    public function setAnchor($anchor): Req {

        $parsedUrl = parse_url($this->uri);

        $parsedUrl['fragment'] = $anchor;

        $this->uri = self::unparseUrl($parsedUrl);

        return $this;

    }

    public function getAnchor(): string {

        $parse = parse_url($this->uri);
        
        return $parse['fragment'] ?? '';

    }

    public function getQueryString(): string {

        $parse = parse_url($this->uri);

        return isset($parse['query']) ? $parse['query'] : '';

    }

    public function getQueryParams(): array {

        $params = [];

        parse_str($this->getQueryString(), $params);

        return $params;

    }

    public function getQueryParam($name, $defaultValue = null) {

        $params = $this->getQueryParams();

        if(!isset($params[$name]) && $defaultValue === null)
            throw new \Exception('invalid query param "'.$name.'"', 400);
        
        return $params[$name] ?? $defaultValue;

    }

    public function getCookies(): array {
        
        $cookies = [];

        $header = $this->getHeader('Cookie', '');

        if(!$header)
            return $cookies;
            
        $header = explode(';', $header);

        foreach($header as $cookie) {

            $cookie = explode('=', $cookie);

            $cookieName = array_shift($cookie);
            $cookieValue = array_shift($cookie);
            
            $cookies[trim($cookieName)] = urldecode($cookieValue);

        }

        return $cookies;

    }

    public function getCookie($cookieName, $defaultValue = null) {

        $cookies = $this->getCookies();

        return isset($cookies[$cookieName]) ? $cookies[$cookieName] : $defaultValue;

    }

    public function send($userOptions = []): Res {

        // Overwritable options by userOptions
        $defaultOptions = array(
            CURLOPT_TIMEOUT_MS => Conf::get('curl.default_timeout_ms', 3000),
            CURLOPT_CONNECTTIMEOUT_MS => Conf::get('curl.default_connecttimeout_ms', 1000),
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        );

        // Forced options
        $requestOptions = array(
            CURLOPT_URL => $this->uri,
            CURLOPT_HEADER => 1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => $this->protocol,
            CURLOPT_CUSTOMREQUEST => $this->verb,
            CURLOPT_HTTPHEADER => array_map(function($headerName, $headerValues) { return $headerName.': '.implode(' ', $headerValues); }, array_keys($this->headers), $this->headers),
            CURLOPT_POSTFIELDS => $this->body,
        );

        $curlOptions = ($defaultOptions + $userOptions + $requestOptions);

        $timeout = isset($curlOptions[CURLOPT_TIMEOUT_MS]) ? (int) $curlOptions[CURLOPT_TIMEOUT_MS] : null;
        $connectTimeout = isset($curlOptions[CURLOPT_CONNECTTIMEOUT_MS]) ? (int) $curlOptions[CURLOPT_CONNECTTIMEOUT_MS] : null;

        $ch = curl_init();
        
        curl_setopt_array($ch, $curlOptions);

        $result = curl_exec($ch);

        $infos = curl_getinfo($ch);

        if($result === false) {
         /*   
            if($timeout && $connectTimeout && $connectTimeout <= $timeout) {
                
                $length = $infos['total_time'];
                
                $newTimeout = $timeout - ($length * 1000);
             
                $userOptions[CURLOPT_TIMEOUT_MS] = $newTimeout;

                if($connectTimeout < $newTimeout) {
                    
                    $this->retries++;
                    
                    return $this->send($userOptions);
                    
                }
                
            }
            */
            throw new \Exception('request curl error : '.curl_error($ch).', url: '.$this->url, 500);
            
        }

        curl_close($ch);
  
        return Res::fromString($result);

    }

}