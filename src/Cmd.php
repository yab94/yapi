<?php

namespace Yapi;

use Yapi;

class Cmd {

    use Tool;

    static public function execute(string $command, array $args = []): string {
    
        $args = array_map('escapeshellarg', $args);
    
        $command = $command.' '.implode(' ', $args);
    
        $output = shell_exec($command);
        
        $output = trim($output);
    
        Log::debug('Cmd::execute "'.$command.'" '.$output);
        
        return $output;
        
    }

    static public function stdin(\Closure $closure = null): string {

        if(PHP_SAPI != 'cli')
            throw new \Exception('stdin can only be used in cli-mode');

        $input = '';

        $stdin = fopen("php://stdin", "r");

        if(!is_resource($stdin))
            throw new \Exception('unable to read stdin');

        while(!feof($stdin)) {

            $line = fgets($stdin);

            $input .= ($closure !== null) ? $closure($line) : $line;

        }

        fclose($stdin);

        return $input;

    }

}